/*
  Install in folder outside of the cql-js source code.
    npm install <path-to-cql-js>

  Run with:
    node nodejs_import.mjs
  Might require "--experimental-modules"?

  In Node REPL:
    let CQL;  // module
    import('@textplus/contextual-query-language-parser').then(module => { CQL = module });
  Or
    let CQL = await import('@textplus/contextual-query-language-parser');
  Then access with:
    CQL.default.CQLParser
*/

// --------------------------------------------------------------------------

import CQL from "@textplus/contextual-query-language-parser";
//import * as CQLLoad from "@textplus/contextual-query-language-parser";
//const CQL = CQLLoad.default ? CQLLoad.default : CQLLoad;
console.debug(CQL);

const CQLParser = CQL.CQLParser;
const CQLError = CQL.CQLError;
const ValidationError = CQL.ValidationError;

var cp = new CQLParser();

cp.parse("cat AND lemma = NOUN");

console.log("Normal query:");
console.log("FQ: %s", cp.toFQ());

// --------------------------------------------------------------------------

console.log("\n".padEnd(79, "-"));
console.log("Syntax error:");
try {
  cp.parse("cat dog");
} catch (e) {
  console.log(e);
  if (e instanceof CQLError) {
    console.log("Is a CQLError (syntax error)!");
  }
}

console.log("\n".padEnd(79, "-"));
console.log("Validation error:");
try {
  cp.parse("cat AND lemmata = dog");
  cp.validate();
} catch (e) {
  console.log(e);
  if (e instanceof ValidationError) {
    console.log("Is a ValidationError (lex cql validation)!");
  }
}

// --------------------------------------------------------------------------
