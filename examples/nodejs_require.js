/*
  Install in folder outside of the cql-js source code.
    npm install <path-to-cql-js>

  Run with:
    node nodejs_require.js
*/

// --------------------------------------------------------------------------

const { CQLParser } = require("@textplus/contextual-query-language-parser");

var cp = new CQLParser();

cp.parse("cat AND lemma = NOUN");

console.log("Normal query:");
console.log("FQ: %s", cp.toFQ());

// --------------------------------------------------------------------------

const { CQLError, ValidationError } = require("@textplus/contextual-query-language-parser");

console.log("\n".padEnd(79, "-"));
console.log("Syntax error:");
try {
  cp.parse("cat dog");
} catch (e) {
  console.log(e);
  if (e instanceof CQLError) {
    console.log("Is a CQLError (syntax error)!");
  }
}

console.log("\n".padEnd(79, "-"));
console.log("Validation error:");
try {
  cp.parse("cat AND lemmata = dog");
  cp.validate();
} catch (e) {
  console.log(e);
  if (e instanceof ValidationError) {
    console.log("Is a ValidationError (lex cql validation)!");
  }
}

// --------------------------------------------------------------------------
