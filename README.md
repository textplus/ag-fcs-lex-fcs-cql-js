## Contextual Query Language parser in JavaScript

A hand-written, recursive descent parser of the Contextual Query Language as
used in the standardized information retrieval protocols - SRU.

The parser can be used in places where the front-end query analysis is 
required (e.g search UIs, etc).

CQL specification, including the grammar and detailed description of the
language: [CQL specs](http://www.loc.gov/standards/sru/cql/)

### Examples

#### Browser

Start simple webserver to serve [index.html](index.html) with
  `python3 -m http.server 8000`
and visit http://localhost:8000 in your browser.
Another visualization is at http://localhost:8000/examples/index-highlight.html.

#### NodeJS

See example files in folder [examples/](examples/). To try them, create a
folder outside of this source directory, run
  `npm install <path-to-this-source-folder>`
and optionally copy the example files into the new folder. Then, simply run
  `node <example-file>`.
(See also the header comment in those examples for details.)

