"use strict";

var EXPORTED_SYMBOLS = ["CQLParser"];

//const _EXPORTS = (function() { // to 

var DEFAULT_SERVER_CHOICE_FIELD = 'cql.serverChoice';
var DEFAULT_SERVER_CHOICE_RELATION = 'scr';

function indent(n, c) {
    var s = "";
    for (var i = 0; i < n; i++)
        s = s + c;
    return s;
}

const CQLError = function (message, query, pos, look, val, lval) {
    this.message = message;
    // raw query string
    this.query = query;
    // parser (error) position in raw query
    this.pos = pos;
    // state: s | q | | ()/ | <>= | "'
    this.look = look;
    // value of token (may be (quoted) content, "index"; field or term)
    this.val = val;
    // lowercased this.val
    this.lval = lval;
    this.stack = (new Error()).stack.split("\n").filter(function (l, i) { return i !== 1 }).join("\n").replace("Error", "CQLError");
}
CQLError.prototype = Object.create(Error.prototype);
CQLError.prototype.name = "CQLError";
CQLError.prototype.constructor = CQLError;

const ValidationError = function (message, query, pos, context) {
    this.message = message;
    // raw query string
    this.query = query;
    // parser (error) position in raw query
    this.pos = pos;
    // some context
    this.context = context;
    this.stack = (new Error()).stack.split("\n").filter(function (l, i) { return i !== 1 }).join("\n").replace("Error", "ValidationError");
}
ValidationError.prototype = Object.create(Error.prototype);
ValidationError.prototype.name = "ValidationError";
ValidationError.prototype.constructor = ValidationError;

// --------------------------------------------------------------------------

// CQLModifier
const CQLModifier = function () {
    // name of (relation) modifier
    this.name = null;
    // relation of modifier value (e.g. equals, lower than, etc.)
    this.relation = null;
    // optional value of modifier ("true" if empty)
    this.value = null;
    this._pos = null;
    this._range = null;
}

CQLModifier.prototype = {
    toString: function () {
        return this.name + this.relation + this.value;
    },

    toXCQL: function (n, c) {
        var s = indent(n + 1, c) + "<modifier>\n";
        s = s + indent(n + 2, c) + "<name>" + this.name + "</name>\n";
        if (this.relation != null)
            s = s + indent(n + 2, c)
                + "<relation>" + this.relation + "</relation>\n";
        if (this.value != null)
            s = s + indent(n + 2, c)
                + "<value>" + this.value + "</value>\n";
        s = s + indent(n + 1, c) + "</modifier>\n";
        return s;
    },

    toFQ: function () {
        //we ignore modifier relation symbol, for value-less modifiers
        //we assume 'true'
        var value = this.value.length > 0 ? this.value : "true";
        var s = '"' + this.name + '": "' + value + '"';
        if (this._pos !== null) s += ', "' + this.name + '@pos": ' + this._pos;
        if (this._range !== null) s += ', "' + this.name + '@range": ' + JSON.stringify(this._range);
        return s;
    },

    toAnnotatedASCIIArt: function (prefix, nl, ql, lvl, mtype) {
        let mark = ("".padStart(this._range[0] - 1) + "m".padEnd(this._range[1] - this._range[0] + 1, "-"));
        if (typeof ql !== "undefined")
            mark = mark.padEnd(ql + 1 + lvl) + "[" + mtype + ":mod:" + this._range + "]";
        return nl + prefix + mark;
    },

    getSpan: function () {
        return this._range;
    }
}

// CQLSearchClause
const CQLSearchClause = function (field, fielduri, relation, relationuri,
    modifiers, term, scf, scr) {
    // index or name
    this.field = field;
    // not supported yet? (always empty string)
    this.fielduri = fielduri;
    // relation symbol or string
    this.relation = relation;
    // not supported yet? (always empty string)
    this.relationuri = relationuri;
    // list of modifiers
    this.modifiers = modifiers;
    // value
    this.term = term;
    this.scf = scf || DEFAULT_SERVER_CHOICE_FIELD;
    this.scr = scr || DEFAULT_SERVER_CHOICE_RELATION;
    // start position of search clause
    this._pos = null;
    // range (start + end position) of search clause (without surrounding braces or spaces)
    this._range = null;
    // start position of relation
    this._relpos = null;
}

CQLSearchClause.prototype = {
    toString: function () {
        var field = this.field;
        var relation = this.relation;
        if (field == this.scf && relation == this.scr) {
            //avoid redundant field/relation
            field = null;
            relation = null;
        }
        return (field ? field + ' ' : '') +
            (relation ? relation : '') +
            (this.modifiers.length > 0 ? '/' + this.modifiers.join('/') : '') +
            (relation || this.modifiers.length ? ' ' : '') +
            '"' + this.term + '"';
    },

    toXCQL: function (n, c) {
        var s = indent(n, c) + "<searchClause>\n";
        if (this.fielduri.length > 0) {
            s = s + indent(n + 1, c) + "<prefixes>\n" +
                indent(n + 2, c) + "<prefix>\n" +
                indent(n + 3, c) + "<identifier>" + this.fielduri +
                "</identifier>\n" +
                indent(n + 2, c) + "</prefix>\n" +
                indent(n + 1, c) + "</prefixes>\n";
        }
        s = s + indent(n + 1, c) + "<index>" + this.field + "</index>\n";
        s = s + indent(n + 1, c) + "<relation>\n";
        if (this.relationuri.length > 0) {
            s = s + indent(n + 2, c) +
                "<identifier>" + this.relationuri + "</identifier>\n";
        }
        s = s + indent(n + 2, c) + "<value>" + this.relation + "</value>\n";
        if (this.modifiers.length > 0) {
            s = s + indent(n + 2, c) + "<modifiers>\n";
            for (var i = 0; i < this.modifiers.length; i++)
                s = s + this.modifiers[i].toXCQL(n + 2, c);
            s = s + indent(n + 2, c) + "</modifiers>\n";
        }
        s = s + indent(n + 1, c) + "</relation>\n";
        s = s + indent(n + 1, c) + "<term>" + this.term + "</term>\n";
        s = s + indent(n, c) + "</searchClause>\n";
        return s;
    },

    toFQ: function () {
        var s = '{"term": "' + this.term + '"';
        if (this.field.length > 0 && this.field != this.scf)
            s += ', "field": "' + this.field + '"';
        if (this.relation.length > 0 && this.relation != this.scr)
            s += ', "relation": "' + this._mapRelation(this.relation) + '"';
        for (var i = 0; i < this.modifiers.length; i++) {
            //since modifiers are mapped to keys, ignore the reserved ones
            if (this.modifiers[i].name == "term"
                || this.modifiers[i].name == "field"
                || this.modifiers[i].name == "relation")
                continue;
            s += ', ' + this.modifiers[i].toFQ();
        }
        if (this._pos !== null) s += ', "@pos": ' + this._pos;
        if (this._range !== null) s += ', "@range": ' + JSON.stringify(this._range);
        if (this._relpos !== null) s += ', "relation@pos": ' + this._relpos;
        s += '}';
        return s;
    },

    toAnnotatedASCIIArt: function (prefix, nl, ql, lvl) {
        lvl = typeof lvl == "undefined" ? 0 : lvl;
        var s = "";
        if (this._range !== null) {
            let mark = ("".padStart(this._range[0] - 1) + "t".padEnd(this._range[1] - this._range[0] + 1, "-"));
            if (typeof ql !== "undefined")
                mark = mark.padEnd(ql + 1 + lvl) + "[term:" + this._range + "]";
            s += nl + prefix + mark;
        }
        if (this._relpos !== null) {
            let relmodend = Math.max(...this.modifiers.map((x) => x._range[1]));
            let mark = ("".padStart(this._relpos - 1) + "r".padEnd(relmodend - this._relpos + 1, "-"));
            if (typeof ql !== "undefined")
                mark = mark.padEnd(ql + 1 + lvl + 1) + "[term:rel:" + this.relation + ":" + this._relpos + "]"
            s += nl + prefix + mark;
        }
        for (var i = 0; i < this.modifiers.length; i++)
            s += this.modifiers[i].toAnnotatedASCIIArt(prefix, nl, ql, lvl + 2, "term");
        return s;
    },

    _mapRelation: function (rel) {
        switch (rel) {
            case "<": return "lt";
            case ">": return "gt";
            case "=": return "eq";
            case "<>": return "ne";
            case ">=": return "ge";
            case "<=": return "le";
            default: return rel;
        }
    },

    _remapRelation: function (rel) {
        switch (rel) {
            case "lt": return "<";
            case "gt": return ">";
            case "eq": return "=";
            case "ne": return "<>";
            case "ge": return ">=";
            case "le": return "<=";
            default: return rel;
        }
    },

    getSpan: function () {
        return this._range;
    }
}

// CQLBoolean
const CQLBoolean = function () {
    // operator: and | or | not | prox
    this.op = null;
    // list of modifiers
    this.modifiers = null;
    // left search clause
    this.left = null;
    // right search clause
    this.right = null;
    this._pos = null;
    this._range = null;
}

CQLBoolean.prototype = {
    toString: function () {
        return (this.left.op ? '(' + this.left + ')' : this.left) + ' ' +
            this.op.toUpperCase() +
            (this.modifiers.length > 0 ? '/' + this.modifiers.join('/') : '') +
            ' ' + (this.right.op ? '(' + this.right + ')' : this.right);
    },

    toXCQL: function (n, c) {
        var s = indent(n, c) + "<triple>\n";
        s = s + indent(n + 1, c) + "<boolean>\n" +
            indent(n + 2, c) + "<value>" + this.op + "</value>\n";
        if (this.modifiers.length > 0) {
            s = s + indent(n + 2, c) + "<modifiers>\n";
            for (var i = 0; i < this.modifiers.length; i++)
                s = s + this.modifiers[i].toXCQL(n + 2, c);
            s = s + indent(n + 2, c) + "</modifiers>\n";
        }
        s = s + indent(n + 1, c) + "</boolean>\n";
        s = s + indent(n + 1, c) + "<leftOperand>\n" +
            this.left.toXCQL(n + 2, c) + indent(n + 1, c) + "</leftOperand>\n";

        s = s + indent(n + 1, c) + "<rightOperand>\n" +
            this.right.toXCQL(n + 2, c) + indent(n + 1, c) + "</rightOperand>\n";
        s = s + indent(n, c) + "</triple>\n";
        return s;
    },

    toFQ: function (n, c, nl) {
        var s = '{"op": "' + this.op + '"';
        //proximity modifiers
        for (var i = 0; i < this.modifiers.length; i++)
            s += ', ' + this.modifiers[i].toFQ();
        s += ',' + nl + indent(n, c) + ' "s1": ' + this.left.toFQ(n + 1, c, nl);
        s += ',' + nl + indent(n, c) + ' "s2": ' + this.right.toFQ(n + 1, c, nl);
        if (this._pos !== null) s += ',' + nl + indent(n, c) + ' "@pos": ' + this._pos;
        if (this._range !== null) s += ',' + nl + indent(n, c) + ' "@range": ' + JSON.stringify(this._range);
        var fill = n && c ? ' ' : '';
        s += nl + indent(n - 1, c) + fill + '}';
        return s;
    },

    toAnnotatedASCIIArt: function (prefix, nl, ql, lvl) {
        lvl = typeof lvl == "undefined" ? 0 : lvl;
        var s = "";
        if (this._range !== null) {
            let mark = ("".padStart(this._range[0] - 1) + "o".padEnd(this._range[1] - this._range[0] + 1, "-"));
            if (typeof ql !== "undefined")
                mark = mark.padEnd(ql + 1 + lvl) + "[op:" + this._range + "]";
            s += nl + prefix + mark;
        } else if (this._pos !== null) {
            let range = [this._pos, this._pos + this.op.length - 1]
            let mark = ("o".padEnd(this.op.length, "-").padStart(this._pos + this.op.length - 1));
            if (typeof ql !== "undefined")
                mark = mark.padEnd(ql + 1 + lvl) + "[op:" + range + "]";
            s += nl + prefix + mark;
        }
        for (var i = 0; i < this.modifiers.length; i++)
            s += this.modifiers[i].toAnnotatedASCIIArt(prefix, nl, ql, lvl + 1, "op");
        s += this.left.toAnnotatedASCIIArt(prefix, nl, ql, lvl + 1);
        s += this.right.toAnnotatedASCIIArt(prefix, nl, ql, lvl + 1);
        return s;
    },

    getSpan: function () {
        return [this.left.getSpan()[0], this.right.getSpan()[1]];
    }
}

// CQLParser
const CQLParser = function () {
    // index/position in raw query
    this.qi = null;
    // raw query string length (char count)
    this.ql = null;
    // raw query string
    this.qs = null;
    // parser state: s | q | ()/ | <>= | "'
    // if empty string then error
    this.look = null;
    // lowercased this.val
    this.lval = null;
    // value of token (may be (quoted) content, "index"; field or term)
    this.val = null;
    // (internal) start position of expression
    this._exprStart = null;
    // (internal) start position of relation (in expression)
    this._exprRelStart = null;
    this.prefixes = new Object();
    // parsed query
    this.tree = null;
    this.scf = null;
    this.scr = null;
}

CQLParser.prototype = {
    parse: function (query, scf, scr) {
        if (!query)
            throw new CQLError("The query to be parsed cannot be empty", this.qs);
        this.scf = typeof scf != 'string'
            ? DEFAULT_SERVER_CHOICE_FIELD : scf;
        this.scr = typeof scr != 'string'
            ? DEFAULT_SERVER_CHOICE_RELATION : scr;
        this.qs = query;
        this.ql = this.qs.length;
        this.qi = 0;
        this.lval = this.val = this._exprStart = null;
        this._move();
        this.tree = this._parseQuery(this.scf, this.scr, new Array());
        if (this.look != "")
            throw new CQLError("EOF expected", this.qs, this.qi, this.look, this.val, this.lval);
    },
    parseFromFQ: function (query, scf, scr) {
        if (!query)
            throw new CQLError("The query to be parsed cannot be empty", this.qs);
        if (typeof query == 'string')
            query = JSON.parse(query);
        this.scf = typeof scf != 'string'
            ? DEFAULT_SERVER_CHOICE_FIELD : scf;
        this.scr = typeof scr != 'string'
            ? DEFAULT_SERVER_CHOICE_RELATION : scr;
        this.tree = this._parseFromFQ(query);
    },
    _parseFromFQ: function (fq) {
        //op-node
        if (Object.prototype.hasOwnProperty.call(fq, 'op')
            && Object.prototype.hasOwnProperty.call(fq, 's1')
            && Object.prototype.hasOwnProperty.call(fq, 's2')) {
            let node = new CQLBoolean();
            node.op = fq.op;
            node.left = this._parseFromFQ(fq.s1);
            node.right = this._parseFromFQ(fq.s2);
            //include all other members as modifiers
            node.modifiers = [];
            for (let key in fq) {
                if (key == 'op' || key == 's1' || key == 's2')
                    continue;
                if (key.endsWith("@pos") || key.endsWith("@range"))
                    continue;
                var mod = new CQLModifier();
                mod.name = key;
                mod.relation = '=';
                mod.value = fq[key];
                node.modifiers.push(mod);
            }
            return node;
        }
        //search-clause node
        if (Object.prototype.hasOwnProperty.call(fq, 'term')) {
            let node = new CQLSearchClause();
            node.term = fq.term;
            node.scf = this.scf;
            node.scr = this.scr;
            node.field = Object.prototype.hasOwnProperty.call(fq, 'field')
                ? fq.field : this.scf;
            node.relation = Object.prototype.hasOwnProperty.call(fq, 'relation')
                ? node._remapRelation(fq.relation) : this.scr;
            //include all other members as modifiers
            node.relationuri = '';
            node.fielduri = '';
            node.modifiers = [];
            for (let key in fq) {
                if (key == 'term' || key == 'field' || key == 'relation')
                    continue;
                if (key.endsWith("@pos") || key.endsWith("@range"))
                    continue;
                let mod = new CQLModifier();
                mod.name = key;
                mod.relation = '=';
                mod.value = fq[key];
                node.modifiers.push(mod);
            }
            return node;
        }
        throw new CQLError('Unknow node type; ' + JSON.stringify(fq), this.qs, this.qi, this.look, this.val, this.lval);
    },
    // -------------------------------
    toXCQL: function (c) {
        c = typeof c == "undefined" ? ' ' : c;
        return this.tree.toXCQL(0, c);
    },
    toFQ: function (c, nl) {
        c = typeof c == "undefined" ? '  ' : c;
        nl = typeof nl == "undefined" ? '\n' : nl;
        return this.tree.toFQ(0, c, nl);
    },
    toString: function () {
        return this.tree.toString();
    },
    toAnnotatedASCIIArt: function (prefix, nl) {
        nl = typeof nl == "undefined" ? '\n' : nl;
        prefix = typeof prefix == "undefined" ? '' : prefix;

        return prefix + this.qs + this.tree.toAnnotatedASCIIArt(prefix, nl, this.ql);
    },
    // -------------------------------
    _parseQuery: function (field, relation, modifiers) {
        var left = this._parseSearchClause(field, relation, modifiers);
        while (this.look == "s" && (
            this.lval == "and" ||
            this.lval == "or" ||
            this.lval == "not" ||
            this.lval == "prox")) {
            var b = new CQLBoolean();
            b._pos = this.qi - this.lval.length + 1;
            b.op = this.lval;
            this._move();
            b.modifiers = this._parseModifiers();
            b._range = [b._pos, Math.max(b._pos + b.op.length - 1, Math.max.apply(null, b.modifiers.map(x => ("_range" in x) ? x._range[1] : 0)))];
            b.left = left;
            b.right = this._parseSearchClause(field, relation, modifiers);
            left = b;
        }
        return left;
    },
    _parseModifiers: function () {
        var ar = new Array();
        let _safeExprStart = this._exprStart;
        let _safeExprRelStart = this._exprRelStart;
        while (this.look == "/") {
            let _mstart = this.qi;
            this._move();
            if (this.look != "s" && this.look != "q")
                throw new CQLError("Invalid modifier.", this.qs, this.qi, this.look, this.val, this.lval)

            var name = this.lval;
            let _mpos = this.qi - this.lval.length + 1;
            let _mend = this.qi;
            this._move();
            if (this.look.length > 0
                && this._strchr("<>=", this.look.charAt(0))) {
                var rel = this.look;
                this._move();
                _mend = this.qi;
                if (this.look != "s" && this.look != "q")
                    throw new CQLError("Invalid relation within the modifier.", this.qs, this.qi, this.look, this.val, this.lval);

                let m = new CQLModifier();
                m._pos = _mpos;
                m._range = [_mstart, _mend];
                this._exprStart = null;
                m.name = name;
                m.relation = rel;
                m.value = this.val;
                ar.push(m);
                this._move();
            } else {
                let m = new CQLModifier();
                m._pos = _mpos;
                m._range = [_mstart, _mend];
                this._exprStart = null;
                m.name = name;
                m.relation = "";
                m.value = "";
                ar.push(m);
            }
        }
        this._exprStart = _safeExprStart;
        this._exprRelStart = _safeExprRelStart;
        return ar;
    },
    _parseSearchClause: function (field, relation, modifiers) {
        if (this.look == "(") {
            let _qi = this.qi;
            this._move();
            var b = this._parseQuery(field, relation, modifiers);
            if (this.look == ")")
                this._move();
            else
                throw new CQLError("Missing closing parenthesis.", this.qs, _qi, this.look, this.val, this.lval);

            return b;
        } else if (this.look == "s" || this.look == "q") {
            let first = this.val;   // dont know if field or term yet
            if (this._exprStart === null)
                this._exprStart = this.qi - this.val.length + 1;
            let _tend = this.qi;
            this._move();
            if (this.look == "q" ||
                (this.look == "s" &&
                    this.lval != "and" &&
                    this.lval != "or" &&
                    this.lval != "not" &&
                    this.lval != "prox")) {
                let rel = this.val;    // string relation
                this._exprRelStart = this.qi - this.val.length + 1;
                this._move();
                return this._parseSearchClause(first, rel,
                    this._parseModifiers());
            } else if (this.look.length > 0
                && this._strchr("<>=", this.look.charAt(0))) {
                let rel = this.look;   // other relation <, = ,etc
                this._move();
                return this._parseSearchClause(first, rel,
                    this._parseModifiers());
            } else {
                // it's a search term
                var pos = field.indexOf('.');
                var pre = "";
                if (pos != -1)
                    pre = field.substring(0, pos);

                var uri = this._lookupPrefix(pre);
                if (uri.length > 0)
                    field = field.substring(pos + 1);

                pos = relation.indexOf('.');
                if (pos == -1)
                    pre = "cql";
                else
                    pre = relation.substring(0, pos);

                var reluri = this._lookupPrefix(pre);
                if (reluri.Length > 0)
                    relation = relation.Substring(pos + 1);

                var sc = new CQLSearchClause(field,
                    uri,
                    relation,
                    reluri,
                    modifiers,
                    first,
                    this.scf,
                    this.scr);
                sc._range = [this._exprStart, _tend];
                sc._relpos = this._exprRelStart;
                this._exprStart = this._exprRelStart = null;
                return sc;
            }
            // prefixes
        } else if (this.look == ">") {
            this._move();
            if (this.look != "s" && this.look != "q")
                throw new CQLError("Expecting string or a quoted expression.", this.qs, this.qi, this.look, this.val, this.lval);

            let first = this.lval;
            this._move();
            if (this.look == "=") {
                this._move();
                if (this.look != "s" && this.look != "q")
                    throw new CQLError("Expecting string or a quoted expression.", this.qs, this.qi, this.look, this.val, this.lval);

                this._addPrefix(first, this.lval);
                this._move();
                return this._parseQuery(field, relation, modifiers);
            } else {
                this._addPrefix("default", first);
                return this._parseQuery(field, relation, modifiers);
            }
        } else {
            throw new CQLError("Invalid search clause.", this.qs, this.qi, this.look, this.val, this.lval);
        }

    },
    _move: function () {
        //eat whitespace
        while (this.qi < this.ql
            && this._strchr(" \t\r\n", this.qs.charAt(this.qi)))
            this.qi++;
        //eof
        if (this.qi == this.ql) {
            this.look = "";
            return;
        }
        //current char
        var c = this.qs.charAt(this.qi);
        //separators
        if (this._strchr("()/", c)) {
            this.look = c;
            this.qi++;
            //comparitor
        } else if (this._strchr("<>=", c)) {
            this.look = c;
            this.qi++;
            this._exprRelStart = this.qi;
            //comparitors can repeat, could be if
            while (this.qi < this.ql
                && this._strchr("<>=", this.qs.charAt(this.qi))) {
                this.look = this.look + this.qs.charAt(this.qi);
                this.qi++;
            }
            //quoted string
        } else if (this._strchr("\"'", c)) {
            if (this._exprStart === null)
                this._exprStart = this.qi + 1;
            this.look = "q";
            //remember quote char
            var mark = c;
            this.qi++;
            this.val = "";
            var escaped = false;
            while (this.qi < this.ql) {
                if (!escaped && this.qs.charAt(this.qi) == mark)
                    break;
                if (!escaped && this.qs.charAt(this.qi) == '\\')
                    escaped = true;
                else
                    escaped = false;
                this.val += this.qs.charAt(this.qi);
                this.qi++;
            }
            this.lval = this.val.toLowerCase();
            if (this.qi < this.ql)
                this.qi++;
            else //unterminated
                this.look = ""; //notify error
            //unquoted string
        } else {
            this.look = "s";
            this.val = "";
            while (this.qi < this.ql
                && !this._strchr("()/<>= \t\r\n", this.qs.charAt(this.qi))) {
                this.val = this.val + this.qs.charAt(this.qi);
                this.qi++;
            }
            this.lval = this.val.toLowerCase();
        }
    },
    _strchr: function (s, ch) {
        return s.indexOf(ch) >= 0
    },
    _lookupPrefix: function (name) {
        return this.prefixes[name] ? this.prefixes[name] : "";
    },
    _addPrefix: function (name, value) {
        //overwrite existing items
        this.prefixes[name] = value;
    },
    // -------------------------------
    validate: function (validator) {
        validator = typeof validator == "undefined" ? new LexCQLValidator() : validator;
        validator.validate(this);
    }
}

// --------------------------------------------------------------------------

const LexCQLValidator = function () {}

LexCQLValidator.prototype = {
    validate: function (parser) {
        let tree = parser.tree;
        let query = parser.qs;

        this.validateCQLNode(tree, query);
    },

    validateCQLNode: function (node, query) {
        if (node instanceof CQLBoolean) {
            this.validateCQLBoolean(node, query);
        } else if (node instanceof CQLSearchClause) {
            this.validateCQLSearchClause(node, query);
        } else {
            throw new ValidationError(
                "Unknown CQL node type? -> " + (typeof node),
                query, node._pos, node
            );
        }
    },

    validateCQLBoolean: function (node, query) {
        const ALLOWED = {
            // operators
            "AND": {
                // modifiers
            },
            "OR": {
                // modifiers
            }
        };

        if (!(node.op.toUpperCase() in ALLOWED)) {
            throw new ValidationError(
                "Unknown boolean operator '" + node.op.toUpperCase() + "'. Not in list of allowed operators: " + Object.keys(ALLOWED),
                query, node._pos, node
            );
        }
        this.validateCQLNode(node.left, query);
        this.validateCQLNode(node.right, query);
    },

    validateCQLSearchClause: function (node, query) {
        const ALLOWED = {
            // fields
            "lemma": {
                // relations
                "=": {
                    // modifiers
                    // "modifiers": {}
                }
            },
            "pos": {
                "=": {
                    // values
                    "values": ["ADJ", "ADP", "ADV", "AUX", "CCONJ", "DET", "INTJ", "NOUN", "NUM", "PART", "PRON", "PROPN", "PUNCT", "SCONJ", "SYM", "VERB", "X"]
                }
            },
            "def": {
                "=": {}
            },
            "senseRef": {
                "=": {}
            }
        };

        // check index/field
        if (node.field.length > 0 && node.field != node.scf) {
            if (!(node.field in ALLOWED)) {
                throw new ValidationError(
                    "Unknown field '" + node.field + "'. Not in list of allowed fields: " + Object.keys(ALLOWED),
                    query, node._pos || (node._range) ? node._range[0] : null, node
                );
            }
        }
        if (node.relation.length > 0 && node.relation != node.scr) {
            const ALLOWED_RELATIONS = ALLOWED[node.field];
            console.debug(ALLOWED_RELATIONS);
            if (!(node.relation in ALLOWED_RELATIONS)) {
                throw new ValidationError(
                    "Unknown relation '" + node.relation + "'. Not in list of allowed relations: " + Object.keys(ALLOWED_RELATIONS),
                    query, node._relpos, node
                );
            }

            if ("values" in ALLOWED_RELATIONS[node.relation]) {
                const ALLOWED_VALUES = ALLOWED_RELATIONS[node.relation]["values"];

                if (ALLOWED_VALUES.indexOf(node.term.toUpperCase()) === -1) {
                    throw new ValidationError(
                        "Not allowed value '" + node.term + "' in index '" + node.field + "' with restricted value set!",
                        query, node._pos || (node._range) ? node._range[0] : null, node
                    );
                }
            }

            if ("modifiers" in ALLOWED_RELATIONS[node.relation]) {
                const ALLOWED_MODIFIERS = ALLOWED_RELATIONS[node.relation]["modifiers"];
                for (var i = 0; i < node.modifiers.length; i++) {
                    //since modifiers are mapped to keys, ignore the reserved ones
                    const modname = node.modifiers[i].name;
                    if (modname == "term"
                        || modname == "field"
                        || modname == "relation")
                        continue;
                    if (!(modname in ALLOWED_MODIFIERS)) {
                        throw new ValidationError(
                            "Unknown relation modifier '" + modname + "'. Not in list of allowed modifiers for relations '" + node.relation + "': " + Object.keys(ALLOWED_MODIFIERS),
                            query, node.modifiers[i]._pos, node
                            // node.modifiers[i]
                        );
                    }
                }
            }
        }
    },
}

// --------------------------------------------------------------------------

const _EXPORTS = {
    // main: parser
    CQLParser: CQLParser,
    // constants
    DEFAULT_SERVER_CHOICE_FIELD: DEFAULT_SERVER_CHOICE_FIELD,
    DEFAULT_SERVER_CHOICE_RELATION: DEFAULT_SERVER_CHOICE_RELATION,
    // helpers: parsed objects
    CQLBoolean: CQLBoolean,
    CQLSearchClause: CQLSearchClause,
    CQLModifier: CQLModifier,
    // errors
    CQLError: CQLError,
    // validation
    LexCQLValidator: LexCQLValidator,
    ValidationError: ValidationError,
};

// }());

// --------------------------------------------------------------------------
// compatibility code for npm/browser etc
// see: https://github.com/umdjs/umd/blob/master/templates/returnExports.js

; (function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define([], factory);
    } else if (typeof module === 'object' && module.exports) {
        // Node. Does not work with strict CommonJS, but
        // only CommonJS-like environments that support module.exports,
        // like Node.
        module.exports = factory();
    } else {
        // Browser globals (root is window)
        // I think this might not be necessary since the other objects
        //   are already defined in the global namespace?
        // root.CQLParser = factory();
        Object.assign(root, factory());
    }
}(typeof self !== 'undefined' ? self : this, function () {
    // Just return the values as dictionary to define the module export.
    return _EXPORTS;
}));

// AMD / ReactJS?
//   import CQLParser from "./cql";
// CommonJS / Node
//   const { CQLParser, CQLError } = require("@textplus/contextual-query-language-parser")
// Browser
//   <script type="text/javascript" src="cql.js"></script>
//     everything should be available globally currently
